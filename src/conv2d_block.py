import tensorflow as tf
from keras.layers import Conv2D, MaxPooling2D, Dense, GlobalMaxPooling2D
from tensorflow import Module


class Conv2DBlock(Module):
    def __init__(self, layers, filters):
        super().__init__()
        self.layers = []

        for i in range(layers):
            self.layers.append(Conv2D(
                filters=filters,
                kernel_size=3,
                padding="same",
                activation=tf.nn.relu,
                name=f"conv2d_{layers}x{filters}_{i + 1}"
            ))

    @tf.function
    def __call__(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

